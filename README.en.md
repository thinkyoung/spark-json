For safety consideration,we only accept pull request
# Please issue us if you find an untranslated app

# Spark App Internationalize

Only trasnlate to English for now

## Way to contribute

Go to the repository directory, find the `app.json` of each application. In the same directory， edit `en.app.json`。Translation those need to be translated json. use `check.sh` in the root directory to verify the integrity of `app.json` and `en.app.json`, and submit pr.

Please ignore `applist.json` and `en.applist.json`. It's not necessary to translate them and correct errors. They are composed of `app.json` and `en.app.json`.

The attribute of currently need to be translated：`Name`, `More`.


## Way to sync

https://gitee.com/deepin-community-store/repo_auto_update_script/blob/master/jsonrsy.sh

The existing problems: once updated, software need to be retranslated

### Why we use gitee instead of github?
We are a Chinese Team, which means github is very slow for us to access.Gitee provided  a fast access in China and also support English

Gitee is certificated by OpenAtom Fundation as a relieable platform

